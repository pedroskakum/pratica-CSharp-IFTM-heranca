﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AplicacaoConsoleHeranca
{
    public abstract class Shape
    {
        public abstract string Nome
        {
            get;            
        }

        public virtual double Area()
        {
            return 0;
        }

        public virtual double Volume()
        {
            return 0;
        }

    }
}
