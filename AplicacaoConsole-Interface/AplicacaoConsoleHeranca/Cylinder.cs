﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AplicacaoConsoleHeranca
{
    class Cylinder:Circle
    {
        private double height;

        public double Height
        {
            get { return height; }
            set { height = value; }
        }

        public override string Nome
        {
            get { return "Cylinder (" + base.Nome + ", height: " + Height.ToString("0.0") + ")"; }
        }
        
        public override double Area()
        {
            double AreaLateral;
            double AreaBase;

            AreaBase = base.Area();
            AreaLateral = 2 * Math.PI * Radius * Height;

            return (2 * AreaBase) + AreaLateral;
        }

        public Cylinder() : base()
        {
            Height = 0;
        }

        public Cylinder(double _x, double _y, double _r, double _h):base(_x, _y, _r)
        {
            Height = _h;
        }

        public override double Volume()
        {
            return base.Area() * Height;
        }

    }
}
