﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AplicacaoConsoleHeranca
{
    public class Point:Shape
    {
        private double x;
        private double y;
        
        public Point(){
            X = 0;
            Y = 0;
        }

        public Point(double x, double y)
        {
            X = x;
            Y = y;
        }

        public double X
        {
            set { x = value; }
            get { return x; }
        }

        public double Y
        {
            set { y = value; }
            get { return y; }
        }

        public override string Nome
        {
            get { return "Point (x: " + X.ToString("0.0") + " y: " + Y.ToString("0.0") + ")"; }
        }

        public override string ToString()
        {
            return Nome;
        }

    }
}
