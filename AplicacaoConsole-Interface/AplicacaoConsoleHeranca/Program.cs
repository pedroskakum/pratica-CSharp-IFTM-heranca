﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AplicacaoConsoleHeranca
{
    class Program
    {
        static void Main(string[] args)
        {
            //Criar um vetor vazio de referências à classe base Shape
            Shape[] vetorShapes = new Shape[3];

            vetorShapes[0] = new Point(7, 11);
            vetorShapes[1] = new Circle(22, 8, 3.5);
            vetorShapes[2] = new Cylinder(10, 10, 3.3, 10);

            string saida = "Demonstração de Polimorfismo:";
            //Exibir Nome, Area e Volume para cada objeto de maneira polimórfica
            foreach(Shape forma in vetorShapes)
            {
                saida += "\n\n" + forma.Nome
                      + "\nArea:" + forma.Area().ToString("0.0")
                      + "\nVolume:" + forma.Volume().ToString("0.0");
            }

            Console.Write(saida);
            Console.ReadLine();

        }
    }
}
