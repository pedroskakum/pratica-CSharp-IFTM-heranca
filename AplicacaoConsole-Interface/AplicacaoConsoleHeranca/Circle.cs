﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AplicacaoConsoleHeranca
{
    class Circle:Point
    {
        private double radius;

        public override string Nome
        {
            get { return "Circle (" + base.Nome + ", radius: " + Radius.ToString("0.0") + ")"; }
        }

        public double Radius
        {
            get { return radius; }
            set { radius = value; }
        }

        public Circle() : base()
        {
            Radius = 0;
        }

        public Circle(double _x, double _y, double _r): base(_x,_y)
        {
            Radius = _r;
        }

        public override double Area()
        {
            return Math.PI * Math.Pow(Radius, 2);
        }

        public double Circumference()
        {
            return Math.PI * (Radius * 2);
        }

        public double Diameter()
        {
            return Radius * 2;
        }
        
    }
}
